## STL-Viewer
simple STL File Viewer
<p>usage: stlviewer file, ...  
example: stlviewer STL_data/baby-tux_bin.STL</p>
Entwicklungsumgebung

- Linux Ubuntu 20.04.1
- gcc c++14
- OpenGL Version= 4.6 (Compatibility Profile) Mesa 21.0.3
<hr></hr>

![alt](README.png)